# -*- coding: utf-8 -*-

if 0: # Stuff for Eclipse
    from gluon.dal import DAL
    from gluon.tools import Auth, Service, Crud
    from gluon.validators import IS_MATCH
    from gluon.sqlhtml import SQLTABLE, SQLFORM, URL, Field
    from gluon.http import redirect
    from gluon.languages import translator as T

    db = DAL()
    auth = Auth()
    service = Service()
    crud = Crud()
    
    global db
    global request
    global session
    global response
    from gluon.globals import Request, Session, Response
    
    req = Request()
    req = request
    ses = Session()
    ses = session
    resp = Response()
    resp = response

'''
def index():
    groups_list = SQLTABLE(db().select(db.picnics.ALL, db.event.ALL,
                                       left=db.event.on(db.event.group_id == db.picnics.id),
                                       orderby=db.picnics.group_name))
    group_list = db(db.picnics).select(orderby=db.picnics.group_name)
    return locals()
'''

def index():
    if len(request.args): page = int(request.args[0])
    else: page = 0
    items_per_page = 2
    limitby = (page * items_per_page, (page + 1) * items_per_page + 1)
    
    group_list = db(db.picnics).select(orderby=db.picnics.group_name, limitby=limitby)
    
    create = crud.create(db.picnics)
    
    search_form = SQLFORM.factory(Field('Search', requires=IS_MATCH('^[\w| ]*$')))
    parent = search_form.element('input[type=submit]').parent
    parent.components = [DIV(TAG.BUTTON(TAG.I(_class='icon-search'), _class='btn'))]

    search_result = (None, False)
    if search_form.process().accepted:
        session.search_term = search_form.vars.Search
        query = (db.picnics.group_name.contains(session.search_term) | 
                  db.picnics.group_desc.contains(session.search_term) | 
                  db.picnics.group_tags.contains(session.search_term))
        search_result = (db(query).select(db.picnics.group_name, db.picnics.group_desc,
                                          db.picnics.group_facebook, db.picnics.group_tags),
                         True)

    return dict(group_list=group_list, page=page, items_per_page=items_per_page,
                create=create, search_form=search_form, search_result=search_result)

@service.jsonrpc
def echo():
    return dict(hello_world="hello_world")

@service.jsonrpc
@service.xml
@service.json
def ls():
    return db(db.picnics).select(orderby=db.picnics.group_name).as_list()

def call():
    return service()

@service.jsonrpc
@service.xml
@auth.requires_login()
def group_create():
    groups_form = crud.create(db.picnics)
    return groups_form

def picnics():
    picnics = db(db.picnics).select(orderby=db.picnics.group_name)
    return locals()

def events():
    picnics = db.picnics(request.args(0)) or redirect(URL('', 'groups'))
    events = db(db.event.picnics == picnics.group_name).select(orderby=db.event.name)
    return locals()

@auth.requires_login()
def picnics_create():
    form = crud.create(db.picnics, next='groups')
    return locals()

@auth.requires_login()
def edit():
    picnics = db.picnics(request.args(0)) or redirect(URL('', 'groups'))
    form = crud.update(db.picnics, picnics, next='groups')
    return locals()

@auth.requires_login()
def event_create():
    db.event.picnics.default = request.args(0)
    form = crud.create(db.event, next='groups')
    return locals()

@auth.requires_login()
def event_edit():
    event = db.event(request.args(0)) or redirect(URL('', 'groups'))
    form = crud.update(db.event, event, next='groups')
    return locals()
