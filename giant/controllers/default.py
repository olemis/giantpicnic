# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a samples controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################

from gluon.storage import Storage

if 0: # Stuff for Eclipse
    from gluon.dal import DAL
    from gluon.tools import Auth, Service, Crud
    from gluon.sqlhtml import SQLTABLE, TABLE
    from gluon.languages import translator as T

    db = DAL()
    auth = Auth()
    service = Service()
    crud = Crud()
    
    global db
    global request
    global session
    global response
    from gluon.globals import Request, Session, Response
    
    req = Request()
    req = request
    ses = Session()
    ses = session
    resp = Response()
    resp = response

@service.json
def echo(foo):
    return dict(foo=foo)

@auth.requires_login()
@service.csv
@service.xml    
@service.json
def get_users():
    return db(db.auth_user).select(db.auth_user.id, db.auth_user.name)

@service.json
def get_user_by_id(user_id):
    return db(db.auth_user.id==user_id).select(db.auth_user.id, db.auth_user.name)

@service.json
def get_events():
    return db(db.event).select()

@service.json
def get_groups():
    return db(db.picnics).select()

def index():
    try:
        if len(request.args): page=int(request.args[0])
        else: page=0
    except ValueError:
        page=0
    items_per_page=2
    limitby=(page*items_per_page,(page+1)*items_per_page+1)
    #response.flash = T("Welcome to GiantPicnic!")
    user_list = db().select(db.auth_user.ALL, limitby=limitby)
    return dict(user_list=user_list, page=page, items_per_page=items_per_page)

#from gluon.template import render
#response.render(index)

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())

def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)

def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_signature()
def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    return dict(form=crud())
