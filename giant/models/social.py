# -*- coding: utf-8 -*-

from gluon.validators import IS_HTTP_URL, IS_IN_DB, IS_DATETIME_IN_RANGE

if 0: # Stuff for Eclipse
    from gluon.dal import Field
    from gluon.validators import IS_NOT_IN_DB, IS_SLUG, IS_ALPHANUMERIC, IS_IN_SET
    from gluon.html import FORM, INPUT

    global db
    global request
    global session
    global response

    from gluon.tools import Auth
    auth = Auth(db, hmac_key=Auth.get_or_create_key())

    from gluon.globals import Request, Session, Response
    
    req = Request()
    req = request
    ses = Session()
    ses = session
    resp = Response()
    resp = response


'''
db.define_table(
    'album',
    Field('name'),
    Field('owner_id'),
    format='owner_id\\%(name)s'
)

db.define_table(
    'image',
    Field('image', requires=IS_IMAGE())
)
'''

db.define_table(
    'tags',
    Field('tag', notnull=True, default="Monthly meetup", requires=IS_ALPHANUMERIC()),
    Field('category', notnull=True, default='event', requires=IS_IN_SET(['event', 'group', 'user']))
)

db.define_table(
    'picnics',
    Field('group_name', notnull=True, requires=[IS_SLUG(), IS_NOT_IN_DB(db, 'picnics.group_name')]),
    Field('group_desc'),
    #Field('image', requires=IS_IMAGE()),
    Field('date_created', 'datetime', default=request.now, writable=False, readable=False),
    Field('group_tags', notnull=True),
    Field('group_website', requires=IS_HTTP_URL()),
    Field('group_facebook', requires=IS_HTTP_URL()),
    Field('members', requires=IS_IN_DB(db, db.auth_user, '%(name)s (%(email)s)', multiple=True)),
    #Field('owner', auth.user, requires=IS_NOT_EMPTY()),
    format='%(group_name)s'
)   


db.define_table(
    'event',
    Field('event_name', notnull=True),
    Field('event_desc'),
    Field('event_datetime', 'datetime', notnull=True, requires=IS_DATETIME_IN_RANGE(minimum=request.now)),
    #Field('image', requires=IS_IMAGE()),
    Field('date_created', 'datetime', default=request.now, writable=False, readable=False),
    Field('last_modified', 'datetime', update=request.now, default=request.now, writable=False, readable=False),
    Field('event_location'),
    Field('group_id', db.picnics, notnull=True, requires=IS_IN_DB(db, db.picnics, db.picnics)),
    format='%(event_name)s'
)

db.define_table(
    'rsvp_list',
    Field('rsvp', notnull=True, requires=IS_IN_SET(['Yes', 'Maybe', 'No'])),
    Field('event_id', db.event, unique=True, notnull=True, requires=IS_IN_DB(db, db.event, db.event._format)),
    Field('user_id', db.auth_user, unique=True, default=auth.user_id)
    #signature=True
)

db.define_table(
    'group_member_list',
    Field('group_id', db.picnics, unique=True, notnull=True, requires=IS_IN_DB(db, db.picnics, db.picnics._format)),
    Field('user_id', db.auth_user, unique=True, default=auth.user_id)
)
